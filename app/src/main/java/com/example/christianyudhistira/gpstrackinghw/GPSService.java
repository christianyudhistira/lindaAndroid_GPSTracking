package com.example.christianyudhistira.gpstrackinghw;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by christian yudhistira on 05/04/2018.
 */

public class GPSService extends Service implements GPSServiceStat{

    public static final String
            ACTION_LOCATION_BROADCAST = GPSService.class.getName() + "LocationBroadcast",
            EXTRA_LATITUDE = "extra_latitude",
            EXTRA_LONGITUDE = "extra_longitude";

    // service definitions
    private String latMessage;
    private String longMessage;
    private OperationalGPSLocation operationalGPSLocation;

    // buffer definitions
    private ArrayList<HashMap<String, String>> gpsData = new ArrayList<HashMap<String, String>>();
    private int looping = 0;

    @Override
    public void onCreate() {
        operationalGPSLocation = new OperationalGPSLocation(getApplicationContext(), this);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(getApplicationContext(), intent.getStringExtra("tes"), Toast.LENGTH_SHORT).show();

        // start GPS service
        operationalGPSLocation.RequestGPSUpdate();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(getApplicationContext(), "stop", Toast.LENGTH_SHORT).show();

        // stop GPS service
        operationalGPSLocation.RemoveGPSUpdate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationReady() {
        latMessage = String.valueOf(operationalGPSLocation.getLastLatitude());
        longMessage = String.valueOf(operationalGPSLocation.getLastLongitude());

        // send localBroadcast message
        Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
        intent.putExtra(EXTRA_LATITUDE, latMessage);
        intent.putExtra(EXTRA_LONGITUDE, longMessage);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

        // insert GPS location into the Buffer
//        addElement(latMessage, longMessage);
    }

    // store GPS data into the buffer
    private void addElement(String lat, String lng) {
        HashMap<String, String> hashElement = new HashMap<String,String>();
        hashElement.put("lat", lat);
        hashElement.put("long", lng);
        gpsData.add(hashElement);
    }

    // count GPS data in the Buffer
    private int countElement() {
        return gpsData.size();
    }
}

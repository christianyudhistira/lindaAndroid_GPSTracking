package com.example.christianyudhistira.gpstrackinghw;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button startBtn;
    private Button stopBtn;
    private TextView latitude;
    private TextView longitude;

    // permissions definitions
    private static int PERMISSION_ALL = 1;
    private String[] PERMISSIONS = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startBtn = (Button) findViewById(R.id.startButton);
        stopBtn = (Button) findViewById(R.id.stopButton);
        latitude = (TextView) findViewById(R.id.latitude);
        longitude = (TextView) findViewById(R.id.longitude);

        // check permission to access GPS
        if (!hasPermissions(PERMISSIONS))
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
    }

    private boolean hasPermissions( String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null)
            for (String permission : permissions)
                if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
                    return false;

        return true;
    }

    // Start GPS
    public void startButtonPress(View v) {
        Intent intent = new Intent(this, GPSService.class);
        intent.putExtra("tes", "start");
        startService(intent);

        // receive updated GPS info and display it on the layout
        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String lat = intent.getStringExtra(GPSService.EXTRA_LATITUDE);
                        String lng = intent.getStringExtra(GPSService.EXTRA_LONGITUDE);

                        latitude.setText(lat);
                        longitude.setText(lng);
                    }
                }, new IntentFilter(GPSService.ACTION_LOCATION_BROADCAST)
        );
    }

    // Stop GPS
    public void stopButtonPress(View v) {
        Intent intent = new Intent(this, GPSService.class);
        stopService(intent);
    }

}

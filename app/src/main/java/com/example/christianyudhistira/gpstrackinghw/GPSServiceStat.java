package com.example.christianyudhistira.gpstrackinghw;

/**
 * Created by christian yudhistira on 05/04/2018.
 */

public interface GPSServiceStat {
    void onLocationReady();
}

package com.example.christianyudhistira.gpstrackinghw;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

/**
 * Created by christian yudhistira on 05/04/2018.
 */

public class OperationalGPSLocation {

    private Context c;
    private double latitude;
    private double longitude;
    private GPSServiceStat gpsStat;

    private LocationManager locationManager;
    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            gpsStat.onLocationReady();
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    public OperationalGPSLocation(Context c, GPSServiceStat gpsStat) {
        this.c = c;
        this.gpsStat = gpsStat;
        locationManager = (LocationManager) c.getSystemService(Service.LOCATION_SERVICE);
        latitude = 0;
        longitude = 0;
    }

    public void RequestGPSUpdate() {
        if (ActivityCompat.checkSelfPermission(c, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(c, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        } else
            // 5000ms (5s) nya diatur buat nentuin interval tiap update data GPS
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, locationListener);
    }

    public void RemoveGPSUpdate() {
        locationManager.removeUpdates(locationListener);
    }

    public double getLastLatitude() {
        return latitude;
    }

    public double getLastLongitude() {
        return longitude;
    }
}
